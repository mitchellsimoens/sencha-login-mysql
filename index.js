module.exports = {
    get Adapter () {
        return require('./Adapter');
    },

    get operation () {
        return require('./operation');
    }
};
