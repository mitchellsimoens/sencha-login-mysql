module.exports = {
    get get () {
        return require('./get');
    },

    get save () {
        return require('./save');
    }
};
