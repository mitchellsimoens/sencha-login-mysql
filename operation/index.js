module.exports = {
    get forum () {
        return require('./forum');
    },

    get login () {
        return require('./login');
    },

    get sencha () {
        return require('./sencha');
    }
};
